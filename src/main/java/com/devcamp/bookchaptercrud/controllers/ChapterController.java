package com.devcamp.bookchaptercrud.controllers;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.bookchaptercrud.model.CChapter;
import com.devcamp.bookchaptercrud.repository.IChapterRepository;

@RestController
@CrossOrigin
@RequestMapping("/chapter")
public class ChapterController {
    @Autowired
    IChapterRepository chapterRepository;
    @GetMapping("/all")
    public ResponseEntity<List<CChapter>> getAllChapter(){
        try {
            List<CChapter> chapterList = new ArrayList<>();
            chapterRepository.findAll().forEach(chapterList::add);
            return new ResponseEntity<>(chapterList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    
    @GetMapping("detail/{id}")
    public ResponseEntity<CChapter> getChapterById(@PathVariable long id){
        try {
            CChapter chapter = chapterRepository.findById(id);
            if (chapter != null){
                return new ResponseEntity<>(chapter, HttpStatus.OK);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Object> createChapter(@RequestBody CChapter pChapter){
        try {
            CChapter newChapter = new CChapter();
            newChapter.setCode(pChapter.getCode());
            newChapter.setName(pChapter.getName());
            newChapter.setIntroduction(pChapter.getIntroduction());
            newChapter.setTranslator(pChapter.getTranslator());
            newChapter.setPage(pChapter.getPage());
            newChapter.setArticles(pChapter.getArticles());
            CChapter _chapter = chapterRepository.save(newChapter);
            return new ResponseEntity<>(_chapter, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Chapter: "+e.getCause().getCause().getMessage());

        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<CChapter> updateChapterById(@PathVariable("id") long id,  @RequestBody CChapter pChapter){
        try {
            CChapter chapter = chapterRepository.findById(id);
            if (chapter != null){
                chapter.setCode(pChapter.getCode());
                chapter.setName(pChapter.getName());
                chapter.setIntroduction(pChapter.getIntroduction());
                chapter.setTranslator(pChapter.getTranslator());
                chapter.setPage(pChapter.getPage());
                chapter.setArticles(pChapter.getArticles());
                
                return new ResponseEntity<>(chapterRepository.save(chapter), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
	public ResponseEntity<CChapter> deleteChapterById(@PathVariable("id") long id) {
		try {
			chapterRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
