package com.devcamp.bookchaptercrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bookchaptercrud.model.CChapter;

public interface IChapterRepository extends JpaRepository <CChapter,Long>{
    CChapter findById(long id);
}
