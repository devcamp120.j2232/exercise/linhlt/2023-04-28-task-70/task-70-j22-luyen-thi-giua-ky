package com.devcamp.bookchaptercrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.bookchaptercrud.model.CArticle;

public interface IArticleRepository extends JpaRepository<CArticle, Long>{
    CArticle findById(long id);
}
