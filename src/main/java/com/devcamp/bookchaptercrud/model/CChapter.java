package com.devcamp.bookchaptercrud.model;

import java.util.Set;

import javax.persistence.*;

@Entity
@Table (name = "chapter")
public class CChapter {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long id;

    @Column (name = "code", unique = true)
    private String code;

    @Column (name = "name")
    private String name;

    @Column (name = "introduction")
    private String introduction;

    @Column (name = "translator")
    private String translator;

    @Column (name = "page")
    private long page;

    @OneToMany (mappedBy = "chapter", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<CArticle> articles;

    public CChapter() {
    }

    public CChapter(long id, String code, String name, String introduction, String translator, long page,
            Set<CArticle> articles) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.introduction = introduction;
        this.translator = translator;
        this.page = page;
        this.articles = articles;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getTranslator() {
        return translator;
    }

    public void setTranslator(String translator) {
        this.translator = translator;
    }

    public long getPage() {
        return page;
    }

    public void setPage(long page) {
        this.page = page;
    }

    public Set<CArticle> getArticles() {
        return articles;
    }

    public void setArticles(Set<CArticle> articles) {
        this.articles = articles;
    }

    
}
