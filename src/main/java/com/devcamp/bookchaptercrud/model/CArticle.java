package com.devcamp.bookchaptercrud.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table (name = "article")
public class CArticle {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long id;

    @Column (name = "code", unique = true)
    private String code;

    @Column (name = "name")
    private String name;

    @Column (name = "introduction")
    private String introduction;

    @Column (name = "page")
    private long page;

    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn (name = "chapter_id", nullable = false)
    @JsonIgnore
    private CChapter chapter;
    
    public CArticle() {
    }
    public CArticle(long id, String code, String name, String introduction, long page, CChapter chapter) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.introduction = introduction;
        this.page = page;
        this.chapter = chapter;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getIntroduction() {
        return introduction;
    }
    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }
    public long getPage() {
        return page;
    }
    public void setPage(long page) {
        this.page = page;
    }
    public CChapter getChapter() {
        return chapter;
    }
    public void setChapter(CChapter chapter) {
        this.chapter = chapter;
    }

    
}
